# code

Repository where we publish all our code. Consisting primarily of Python scripts to facilitate our tasks in the OfiLibre.

## parse_catalog
Python script that creates a CSV file with necessary data for our OfiLibre certificate model.
It opens all MARKDOWN files in the current directory and parses fields out of the Front Matter section.

## frontmatter_rewriter
This python script transforms the Front Matter section of all posts at once, to adapt it to the new HUGO theme. The input and output frontmatter format is YAML.
The script opens all Markdown files in the current directory, and saves a new version of each .md file in a "/hugo" folder.
Each frontmatter field is parsed and rewritten to the fields expected by the new website.
