# -*- coding: utf-8 -*-
"""
Licensed under GPLv3
Author: Clara E. Palomares Calvo

-- ENG:
This script transforms all posts' frontmatter at once, to adapt it to the new HUGO theme. The frontmatter format is YAML (both for input and output).
The script opens all Markdown files in the current directory, and saves a new version of each .md file in a "/hugo" folder.
Each frontmatter field is parsed and rewritten to the fields expected by the new website.
Requirements: install python-frontmatter extension package.

-- SPA:
Script que transforma el FrontMatter de todos los posts de la Ofilibre a la vez, para adaptarse al nuevo tema de HUGO. Tanto el frontmatter original como el resultante es YAML.  
Este script recorre todos los ficheros Markdown contenidos en la carpeta actual, y crea una carpeta "/hugo" donde guardará una nueva versión de cada fichero .md,
actualizada con los campos de frontmatter que espera recibir el nuevo sitio web.
Requisitos: tener instalado el paquete python-frontmatter.
"""

import frontmatter
import glob
import os

# String that marks the limits of the frontmatter
YAML_MARKER = "---\n"

# Opens all Markdown files in the same folder as the script
md_file_names = glob.glob('./*.md')

for idx, each_md_file in enumerate(md_file_names):
    print("Processing file ", each_md_file, "(", idx+1, " of ", len(md_file_names), ")")
    
    with open(each_md_file, 'r', encoding='utf-8') as f:
    
        post = frontmatter.load(f)
        
        # parse the filename to obtain the publish date and the former url
        y, month, day, rest = each_md_file.split('-',3)
        formerURL = rest.split('.')[0]
        year = y.split('\\')[1] 
        
        if not os.path.exists('hugo'):
            os.makedirs('hugo')
            
        with open("./hugo"+each_md_file, 'w', encoding='utf-8') as out:
            out.write(YAML_MARKER)
            out.write("title: \"" + post['title'] + "\"\n")
            out.write("date: " + year + "-" + month + "-" + day + "\n")
            out.write("slug: " +formerURL +"\n")
            out.write("draft: false\n")
            out.write("author: \"OfiLibre\"\n")
            out.write("type: \"post\"\n")
            
            if ( post['tags'] != [] ):
                out.write("tags: [" + ", ".join(post['tags']) + "]\n")
                out.write("categories: [\"" +post['tags'][0] + "\"]\n")
            
            if (post['excerpt']) is not None:
                out.write("\ndescription: \"" + post['excerpt'] + "\"\n")
            
            out.write("\n# post images \n")
            if (post['image']['feature']) is not None:
                out.write("bg_image: \"images" + post['image']['feature'] + "\"\n")
                out.write("feature: \"images" + post['image']['feature'] + "\"\n")
            else: 
                out.write("feature:\n")
            if (post['image']['thumb']) is not None:
                out.write("thumb: \"images" + post['image']['thumb'] + "\"\n")
            else:
                out.write("thumb:\n")
            
            out.write(YAML_MARKER)
            out.write(post.content)
            out.close()

            print("Created Hugo version of post.")
