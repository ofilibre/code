# -*- coding: utf-8 -*-
"""
Licensed under GPLv3.
Author: Clara E. Palomares Calvo

--
This script creates a CSV file with necessary data for our OfiLibre certificate model.
It opens all MARKDOWN files in the current directory and parses fields out of the Front Matter section.
Requirements: installing python-frontmatter extension package.

--
Script que genera un fichero CSV con los datos requeridos por el modelo de certificado.
Recorre todos los ficheros MARKDOWN contenidos en el directorio actual.
Extrae de la sección Front Matter los campos necesarios, y los escribe en un fichero .csv
Para ejecutar este script, es necesario tener instalado el paquete python-frontmatter.
"""

import frontmatter
import csv
import glob

# Opens all Markdown files in the same folder as the script
md_file_names = glob.glob('./*.md')

rows_to_write = []

for idx, each_md_file in enumerate(md_file_names):
    print("Processing file ", each_md_file, "(", idx+1, " of ", len(md_file_names), ")")
    with open(each_md_file, 'r', encoding='utf-8') as f:
        data = frontmatter.load(f)
   
        # Adds a new row to the CSV: title, authors, date, url
        rows_to_write.append([data['title'], data['autores'], data['date'], data['enlace']])

with open('Materiales.csv', 'w', newline='') as out:
    csv_writer = csv.writer(out, delimiter=',', quotechar='"')
    csv_writer.writerow(['Titulo_materiales', 'Autores', 'Fecha', 'url'])
    csv_writer.writerows(rows_to_write)
    print("File Materiales.csv created.")
    
